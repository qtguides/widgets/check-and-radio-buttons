#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setWindowState(Qt::WindowMaximized);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    QString viajo("<b>Viajo en:</b> ");
    if(ui->radioButton1->isChecked())
    {
        viajo += ui->radioButton1->text();
    } else if(ui->radioButton2->isChecked()) {
        viajo += ui->radioButton2->text();
    } else if(ui->radioButton3->isChecked()) {
        viajo += ui->radioButton3->text();
    }

    QString almuerzo("<b>Hoy almuerzo:</b> ");
    if(ui->checkBox1->isChecked()) {
        almuerzo += ui->checkBox1->text() + " ";
    }
    if(ui->checkBox2->isChecked()) {
        almuerzo += ui->checkBox2->text() + " ";
    }
    if(ui->checkBox3->isChecked()) {
        almuerzo += ui->checkBox3->text() + " ";
    }
    if(almuerzo.length() < 22) {
        almuerzo += "nada!";
    }

    QMessageBox::information(this, "Titulo", viajo + "<br>" + almuerzo);
}
